<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCentreDeRecherchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centre_de_recherches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo')->nullable();
            $table->string('image_cover')->nullable();
            $table->string('nom');
            $table->string('sigle');
            $table->integer('universite_id')->nullable();
            $table->text('description')->nullable();
            $table->longText('contenu')->nullable();
            $table->boolean('etat')->nullable();
            $table->index(['id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('centre_de_recherches');
    }
}
