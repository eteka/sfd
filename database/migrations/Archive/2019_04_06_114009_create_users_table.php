<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('prenom')->nullable();
            $table->string('photo')->nullable();
            $table->string('telephone')->nullable();
            $table->char('sexe')->nullable();
            $table->longText('biographie')->nullable();
            $table->date('date_nais')->nullable();
            $table->string('email')->nullable();
            $table->integer('universite_id')->nullable();
            $table->integer('titre_id')->nullable();
            $table->integer('affiliation_id')->nullable();
            $table->index(['id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
