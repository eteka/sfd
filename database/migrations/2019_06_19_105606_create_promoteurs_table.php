<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromoteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promoteurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('denomination');
            $table->string('adresse')->nullable();
            $table->string('telephone')->nullable();
            $table->string('email')->nullable();
            $table->date('datecreation')->nullable();
            $table->string('ville')->nullable();
            $table->longText('assuranceentreprise')->nullable();
            $table->string('licence')->nullable();
            $table->integer('type_id')->nullable();
            $table->string('surnom')->nullable();
            $table->string('nom');
            $table->string('prenom')->nullable();
            $table->char('sexe')->nullable();
            $table->string('situationmatrimoniale')->nullable();
            $table->string('nompere')->nullable();
            $table->string('nommere')->nullable();
            $table->string('niveau_etude')->nullable();
            $table->integer('nb_enfant_a_charge')->nullable();
            $table->integer('nb_autrepersone_a_charge')->nullable();
            $table->boolean('etat')->nullable();
            $table->index(['id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promoteurs');
    }
}
