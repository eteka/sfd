<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
    	
        return Validator::make($data, [
                    'nom' => 'required|string|min:3|max:100',
                    'prenom' => 'required|string|min:4|max:100',
                    'email' => 'required|email|min:4|max:100|unique:users',
                    
                    'tel' => 'required|numeric|digits:8',
                    'universite' => 'required|integer|min:1',
                    'affiliation' => 'required|integer|min:1',
                    'affiliation' => 'required|integer|min:1',
                    'date_nais' => 'required',
                    'sexe' => 'required|in:H,F',
                    //'telephone' => 'required|min:8',
					'password' => 'required|string|min:6|confirmed',
        ]);
       // dd($data);
    }
    
    public function generatePseudo($n,$p){
       $n1=substr($n,0,15);
       $pr1=substr($p,0,15);
        $i=0;
        do {

            if($i>0){
             $pseudo=str_slug($n1." ".$pr1,'.').''.$i;
            }else{
                $pseudo=str_slug($n." ".$p,'.'); 
            }
           
            $nb=User::Where('profile',$pseudo)->count();
            $i++;
        }while($nb>0);
        return $pseudo;
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        $data['photo']=isset($data['photo'])?$data['photo']:'';
        $tpren=explode(" ", $data['prenom']);
        $pren1=count($tpren)>=1?$tpren[0]:'';
        $profile=$this->generatePseudo($pren1,$data['nom']);
        $data['profile']=$profile;
        $userNew=[
                    'nom' => $data['nom'],
                    'prenom' => $data['prenom'],
                    'sexe' => $data['sexe'],
                    'photo' => '',
                    'profile' => $data['profile'],
                    'pcode' => str_random(10),
                    'date_nais' => $data['date_nais'],
                    'telephone' => $data['tel'],
                    'titre_id' => $data['titre'],
                    'universite_id' => $data['universite'],
                    'affiliation_id' => $data['affiliation'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ];
        //dd($userNew);
        if($profile!=NULL){
        return User::create($userNew);
		}
    	else {
    		abort(404);}
    }

}
