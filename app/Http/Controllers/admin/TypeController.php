<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Type;
use Illuminate\Http\Request;
use Session;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $type = Type::where('nom', 'LIKE', "%$keyword%")
				->orWhere('date_type', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $type = Type::paginate($perPage);
        }

        return view('admin.type.index', compact('type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        return view('admin.type.create',compact('replace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'min:3|max:200',
			'date_type' => 'date'
		]);
        $requestData = $request->all();
        
        Type::create($requestData);

        Session::flash('flash_message', 'Type ajouté avec succès !');

        return redirect('admin/type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $type = Type::findOrFail($id);
        $replace="/".$id;
        return view('admin.type.show', compact('type','replace'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $type = Type::findOrFail($id);
        $replace="/".$id."/edit";
        return view('admin.type.edit', compact('type','replace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nom' => 'min:3|max:200',
			'date_type' => 'date'
		]);
        $requestData = $request->all();
        
        $type = Type::findOrFail($id);
        $type->update($requestData);

        Session::flash('flash_message', 'La Mise à jour de "Type" a été effectuée  !');

        return redirect('admin/type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Type::destroy($id);

        Session::flash('flash_message', 'La suppression de "Type" a été effectuée !');

        return redirect('admin/type');
    }
}
