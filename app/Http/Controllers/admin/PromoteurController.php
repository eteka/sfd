<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Promoteur;
use Illuminate\Http\Request;
use Session;

class PromoteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $promoteur = Promoteur::where('denomination', 'LIKE', "%$keyword%")
				->orWhere('adresse', 'LIKE', "%$keyword%")
				->orWhere('telephone', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('datecreation', 'LIKE', "%$keyword%")
				->orWhere('ville', 'LIKE', "%$keyword%")
				->orWhere('assuranceentreprise', 'LIKE', "%$keyword%")
				->orWhere('licence', 'LIKE', "%$keyword%")
				->orWhere('type_id', 'LIKE', "%$keyword%")
				->orWhere('surnom', 'LIKE', "%$keyword%")
				->orWhere('nom', 'LIKE', "%$keyword%")
				->orWhere('prenom', 'LIKE', "%$keyword%")
				->orWhere('sexe', 'LIKE', "%$keyword%")
				->orWhere('situationmatrimoniale', 'LIKE', "%$keyword%")
				->orWhere('nompere', 'LIKE', "%$keyword%")
				->orWhere('nommere', 'LIKE', "%$keyword%")
				->orWhere('niveau_etude', 'LIKE', "%$keyword%")
				->orWhere('nb_enfant_a_charge', 'LIKE', "%$keyword%")
				->orWhere('nb_autrepersone_a_charge', 'LIKE', "%$keyword%")
				->orWhere('etat', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $promoteur = Promoteur::paginate($perPage);
        }

        return view('admin.promoteur.index', compact('promoteur'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        
        $replace="/create";
        return view('admin.promoteur.create',compact('replace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'min:3|max:250',
			'denomination' => 'min:2|max:20'
		]);
        $requestData = $request->all();
        
        Promoteur::create($requestData);

        Session::flash('flash_message', 'Promoteur ajouté avec succès !');

        return redirect('admin/promoteur');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $promoteur = Promoteur::findOrFail($id);
        $replace="/".$id;
        return view('admin.promoteur.show', compact('promoteur','replace'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $promoteur = Promoteur::findOrFail($id);
        $replace="/".$id."/edit";
        return view('admin.promoteur.edit', compact('promoteur','replace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nom' => 'min:3|max:250',
			'denomination' => 'min:2|max:20'
		]);
        $requestData = $request->all();
        
        $promoteur = Promoteur::findOrFail($id);
        $promoteur->update($requestData);

        Session::flash('flash_message', 'La Mise à jour de "Promoteur" a été effectuée  !');

        return redirect('admin/promoteur');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Promoteur::destroy($id);

        Session::flash('flash_message', 'La suppression de "Promoteur" a été effectuée !');

        return redirect('admin/promoteur');
    }
}
