<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User ;
use \App\Models\Publication as Article;
use \App\Models\CentreDeRecherche;
use \App\Models\Discipline;
use \App\Models\Publication;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

   
    public function index()
    {

        
        return view('admin.index');
    }
    public function admin_credential_rules(array $data)
    {
        $messages = [
        'current-password.required' => 'Please enter current password',
        'password.required' => 'Please enter password',
        ];
        $validator = Validator::make($data, [
        'current-password' => 'required',
        'password' => 'required|same:password',
        'password_confirmation' => 'required|same:password',     
        ], $messages);
    
    return $validator;
    }  
    public function getEditPassword($slug)
    { 
        return view('web.edit-password');
    }
    public function postEditPassword(Request $request,$slug)
    { 
        if(!Auth::user()){abort(404);}
        $user=Auth::user();
        if($user->profile==$slug)
        {
            
            $request_data = $request->All();
            
            $validator = $this->validate($request, [
                'current-password' => 'required',
                'password' => 'required|same:password',
                'password_confirmation' => 'required|same:password',
            ]); 
                $current_password = Auth::User()->password;           
                if(Hash::check($request_data['current-password'], $current_password))
                {           
                    $user_id = Auth::User()->id;                       
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['password']);;
                    $obj_user->save(); 
                    Session::flash('success', "Votre mot de passe a été modifié avec succès !");
                }
                else
                {           
                    Session::flash('danger', "Le mot de passe actuel n'est pas valide !");
                    $error = array('current-password' => 'Please enter correct current password');
                    return redirect()->back();   
                }
            
        }else
        {
            abort(503);
        }
        return redirect()->to(route('profile',$user->profile));
    }

        public function getEditProfil($slug)
    { 
        if(!Auth::user()){            
            abort(404);
        }
        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){
            return view('web.edit-profile',compact('user'));
        }
        abort(404);  
    }
    public function getEditBiographie($slug)
    {
        if(!Auth::user()){            
            abort(404);
        }
        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){
            return view('web.edit-biographie',compact('user'));
        }
        abort(404);        
    }
     public function getActivataion($slug){
        $user=User::where("profile",$slug)->firstOrFail();
        if(Auth::user() && Auth::user()->profile!=$slug ){

            Session::flash('warning', "OPERATION NON AUTORISE.<br>Vous n'avez pas l'autorisation requise !");
            return  redirect()->back();
        }
        if($user->etat==1){
             Session::flash('flash_message', "OPERATION NON AUTORISE.<br>Votre compte est déjà activé !");
            return  redirect()->back();
        }
        //dd(Auth::user()->profile);
        return view('web.activation-compte',compact('user'));

     }
      public function pActivataion($slug,Request $request){
        $user=User::where("profile",$slug)->firstOrFail();
       // dd($user);
        if(Auth::user() && Auth::user()->profile!=$slug ){
            Session::flash('warning', "OPERATION NON AUTORISE.<br>Vous n'avez pas l'autorisation requise !");
            return  redirect()->back();
        }
        if($user->etat==1){
             Session::flash('flash_message', "OPERATION NON AUTORISE.<br>Votre compte est déjà activé !");
            return  redirect()->back();
        }
        $requestData = $request->all();
        $scode=trim($requestData['active_code']);
       // dd($scode." ".$user->pcode);
        if($user->pcode==$scode){
                
                
                $user->etat=1;
                $user->update(['etat',1]);
                
                return  redirect(route("profile",$user->profile));
        }else{
           Session::flash('danger', "Le code d'activation n'est pas valide!");
            return  redirect()->back();
        }

      }
    public function postValidate($id,Request $request){
        if(!Auth::user() && Auth::user()->is_admin){            
            abort(404);
        }
        $user=User::where("id",$id)->firstOrFail();

        $requestData = $request->all();
        if(isset($requestData['action']) && $requestData['action']=="_act"){
                //dd($requestData);
                $user->update(['etat'=>1]);
                Session::flash('flash_message', 'Compte Activé avec succès !');
        }elseif(isset($requestData['action']) && $requestData['action']=="_dest"){
            # code...
            $newcode=str_random(10);
            //dd($newcode);
             $user->update(['etat'=>0,'pcode'=>$newcode]);
             Session::flash('flash_message', 'Compte désactivé avec succès !');
        }
       return  redirect()->back();

    }
    public function pEditProfil($slug,Request $request)
    {
        if(!Auth::user()){            
            abort(404);
        }

        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){

            $this->validate($request, [
            'nom' => 'min:3|max:150',
            'prenom' => 'min:3|max:150',
            'adresse' => 'max:250',
            //'sexe' => 'required',
            //'date_nais' => 'required',
            'telephone' => 'max:20',
            'titre_id' => 'required',
            'universite_id' => 'required',
            'affiliation_id' => 'required',
            'photo' => 'image',
        ]);
        $requestData = $request->all();
        

        if ($request->hasFile('photo')) {
            if( $file=$request['photo']){
                $urlimg='/uploads/users/photos/';
                $uploadPath = public_path($urlimg);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo']= $urlimg.$fileName;
//dd('OK');
            }
        }else{
            $requestData['photo']='';
        } 
         $userNew=[
                    'nom' => $requestData['nom'],
                    'prenom' => $requestData['prenom'],
                    'sexe' => $requestData['sexe'],
                    'photo' => $requestData['photo'],
                   // 'profile' => $data['profile'],
                    //'pcode' => str_random(10),
                    //'date_nais' => $requestData['date_nais'],
                    'adresse' => $requestData['adresse'],
                    'telephone' => $requestData['telephone'],
                    'titre_id' => $requestData['titre_id'],
                    'universite_id' => $requestData['universite_id'],
                    'affiliation_id' => $requestData['affiliation_id']
        ];
       //dd($userNew);
       
        $user->update($userNew);

        Session::flash('success', 'La Mise à jour de votre profile a été effectuée  !');

        return redirect()->back();
        return redirect(route('profile',$user->profile));
        }
       // dd($request);
    }
    public function pPubValidate($id,Request $request)
    {
         if(!Auth::user() && Auth::user()->is_admin){            
            abort(404);
        }
        $pub=Article::where("id",$id)->firstOrFail();
       
        $requestData = $request->all();
        if(isset($requestData['action']) && $requestData['action']=="_pact"){
                //dd($requestData);
                $pub->update(['etat'=>1]);
                Session::flash('success', 'Publication activé avec succès !');
        }elseif(isset($requestData['action']) && $requestData['action']=="_pdst"){
            
             $pub->update(['etat'=>0]);
             Session::flash('warning', 'Publication désactivé avec succès !');
        }
       return  redirect()->back();

    }
    public function postEditBio($slug,Request $request)
    {
       
        if(!Auth::user()){            
            abort(404);
        }
        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){
             $this->validate($request, [
            'bio' => 'min:10|string',
            ]);
              $requestData = $request->all();
             $bio=isset($requestData['bio'])?$requestData['bio']:'';
             $user->biographie=$bio;
             $user->save();
             return redirect(route('profile',$user->profile));
           // dd($bio);
        }
        
    }

    
}
