<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User ;
use \App\Models\Publication as Article;
use \App\Models\Discipline;
use \App\Models\Publication;
use \App\Models\TypeDocument;
use \App\Models\Titre;
use App\Models\contact;
use Session;
use Response;
use Auth;


class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {

        
        return view('web.index');
    }
   
}
