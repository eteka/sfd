<?php

namespace App\Http\Middleware;

use Closure;
use \App\User ;
use \App\Models\Publication as Article;
use \App\Models\CentreDeRecherche;
use \App\Models\Discipline;
use \App\Models\Publication;
use \App\Models\TypeDocument;
use Auth;
use View;

class IsAdminMiddleware.php 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

       if(Auth::user() && Auth::user()->is_admin){
        return $next($request);
    }else{
        abort(404);
       
    }
    }
}
