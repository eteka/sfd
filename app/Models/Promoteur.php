<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promoteur extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promoteurs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['denomination', 'adresse', 'telephone', 'email', 'datecreation', 'ville', 'assuranceentreprise', 'licence', 'type_id', 'surnom', 'nom', 'prenom', 'sexe', 'situationmatrimoniale', 'nompere', 'nommere', 'niveau_etude', 'nb_enfant_a_charge', 'nb_autrepersone_a_charge', 'etat'];

    
}
