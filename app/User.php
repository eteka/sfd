<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
      use Notifiable, HasRoles;
         /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom','image','sexe', 'email','date_nais', 'password','pseudo','etat','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getSexe() {
        $sexe='-';
      if($this->sexe=='H'){
        $sexe='Masculin';
    }elseif($sexe=='F'){
       $sexe='Féminin'; 
    }
    
       return$sexe;  
    }
    public function affiliation() {
      return $this->belongsTo('App\Models\CentreDeRecherche', "affiliation_id");  
    }
    public function getUniversite() {
      return "Université de Parakou";  
    }
    public function titre() {
      return $this->belongsTo('App\Models\Titre', "titre_id");  
    }
    public function publications()
    {
        return  $this->belongsToMany('App\Models\Publication', 'publication_user','user_id', 'publication_id')->withTimestamps();
    }
    public function slides() {
      return $this->hasMany('App\Slide', "user_id");  
    }
    public function annonces() {
      return $this->hasMany('App\Annonce', "user_id");  
    }
    public function cartegorie_articles() {
      return $this->hasMany('App\CategorieArticle', "user_id");  
    }
//    public function roles() {
//      return $this->hasMany('App\Role', "user_id");  
//    }
    public function pages() {
      return $this->hasMany('App\Page', "user_id");  
    }
    public function messages() {
      return $this->hasMany('App\Message', "user_id");  
    }
    public function galeries() {
      return $this->hasMany('App\Galerie', "user_id");  
    }
    public function events() {
      return $this->hasMany('App\Event', "user_id");  
    }
    public function documents() {
      return $this->hasMany('App\Document', "user_id");  
    }
}
