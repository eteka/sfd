<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Publication;
use Illuminate\Http\Request;
use \App\USer;
use Session;
use Auth;

class PublicationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('ActiveMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;
        $uid=0;

        if (!empty($keyword)) {
            $publication = Publication::where('url_fichier', 'LIKE', "%$keyword%")
				->orWhere('image_pub', 'LIKE', "%$keyword%")
				->orWhere('titre', 'LIKE', "%$keyword%")
				->orWhere('auteurs', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('annee_pub', 'LIKE', "%$keyword%")
				->orWhere('discipline_id', 'LIKE', "%$keyword%")
				->orWhere('journal', 'LIKE', "%$keyword%")
				->orWhere('categorie_journal', 'LIKE', "%$keyword%")
				->orWhere('impactfactor', 'LIKE', "%$keyword%")
				->orWhere('volume', 'LIKE', "%$keyword%")
				->orWhere('numero', 'LIKE', "%$keyword%")
				->orWhere('resume', 'LIKE', "%$keyword%")
				->orWhere('mots_cle', 'LIKE', "%$keyword%")
				->orWhere('pages', 'LIKE', "%$keyword%")
				->orWhere('centre_id', 'LIKE', "%$keyword%")
				->orWhere('type_document_id', 'LIKE', "%$keyword%")
				->orWhere('etat', 'LIKE', "%$keyword%")
				->orWhere('visibilite_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            if(Auth::user()){
                if(Auth::user()->is_admin==1){
                     $publication = Publication::paginate($perPage);
                     
                 }else{
                    $publication = Auth::user()->publications()->paginate($perPage);
                 }
               
            }
            
        }

        return view('admin.publication.index', compact('publication'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";

        return view('admin.publication.create',compact('replace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titre' => 'min:3|max:255|unique:publications',
            'annee_pub' => 'required|integer',
            #'volume' => 'required|integer',
            #'numero' => 'required|integer',
            'discipline_id' => 'required',
            'journal' => 'required',
            'auteurs' => 'required',
            #'impactfactor' => 'required',
            'resume' => 'required',
            'mots_cle' => 'required',
            'url_fichier' => 'required'
		]);
        $requestData = $request->all();
        $auteurs_array=$requestData['auteurs'];
        $requestData['slug']=substr(str_slug($requestData['annee_pub'].'-'.$requestData['titre'].'-'.$requestData['discipline_id']),0,255);
        $auteurs_id=implode($requestData['auteurs'], ',');
        $requestData['auteurs']=$auteurs_id;

        $tab_centre=[];
        foreach ($auteurs_array as $key => $a) {
            $user=User::where('id',$a)->first();
            if(empty($user)){abort(404);}
            $tab_centre[]=$user->affiliation?$user->affiliation->id:0;
        }

        if ($request->hasFile('url_fichier')) {
            if($file=$request['url_fichier'] ){
                 $urlfile='/uploads/publications/images/';
                $uploadPath = public_path($urlfile);

                $extension = $file->getClientOriginalExtension();
                $fileName = substr(str_slug($requestData['titre']),0,50).'_image_'.rand(11111, 999999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['url_fichier'] = $urlfile.$fileName;
            }
        }


        if ($request->hasFile('image_pub')) {
            if($file=$request['image_pub']){
                $urlfile='/uploads/publications/images/';
                $uploadPath = public_path($urlfile);

                $extension = $file->getClientOriginalExtension();
                $fileName = substr(str_slug($requestData['titre']),0,50).'_file_'.rand(11111, 999999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image_pub'] = $urlfile.$fileName;
            }
        }
        
        //dd($tab_centre);
        $pub=new Publication($requestData);
        $pub->save();
        $user = User::WhereIn('id',$auteurs_array)->firstOrFail();
        $pub->auteurs()->sync($user);
        $pub->labos()->sync($tab_centre);

        Session::flash('flash_message', 'Publication ajouté avec succès !');

        return redirect('administration/publication');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $publication = Publication::findOrFail($id);
        $replace="/".$id;
        return view('admin.publication.show', compact('publication','replace'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if(!Auth::user()){
            return redirect("login");
        }

        $publication = Publication::findOrFail($id);
        $etat=(int)$publication->etat;
       // dd($etat);
        if(!Auth::user()->is_admin && $etat==1){
            Session::flash('warning', 'Opération non autorisée');
            return redirect()->back();
        }
        $replace="/".$id."/edit";
        $auteurs_id = explode(',',$publication['auteurs'] );
        $publication['auteurs']=$auteurs_id;
        //dd($auteurs_id);
        return view('admin.publication.edit', compact('publication','replace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'titre' => 'min:3|max:255',
            'annee_pub' => 'required|integer|min:4',
            #'volume' => 'required|integer',
            #'numero' => 'required|integer',
            'discipline_id' => 'required',
            'journal' => 'required',
            #'categorie_journal' => 'required',
            'auteurs' => 'required',
            'impactfactor' => 'required',
            'resume' => 'required',
            'mots_cle' => 'required'
        ]);
        if(!Auth::user()){
            return redirect("login");
        }
        $publication = Publication::findOrFail($id);
         $etat=(int)$publication->etat;
       // dd($etat);
        if(!Auth::user()->is_admin && $etat==1){
            Session::flash('warning', 'Opération non autorisée');
            return redirect()->back();
        }
        $requestData = $request->all();
        //if(isset($requestData['fslug'])){
            $requestData['slug']=substr(str_slug($requestData['annee_pub'].'-'.$requestData['titre'].'-'.$requestData['discipline_id']),0,255);
       // }
        $auteurs_array=$requestData['auteurs'];
        $auteurs_id=implode($requestData['auteurs'], ',');
        $requestData['auteurs']=$auteurs_id;
         $tab_centre=[];
        foreach ($auteurs_array as $key => $a) {
            $user=User::where('id',$a)->first();
            if(empty($user)){abort(404);}
            $tab_centre[]=$user->affiliation?$user->affiliation->id:0;
        }

        if ($request->hasFile('url_fichier')) {
            if($file=$request['url_fichier'] ){
                 $urlfile='/uploads/publications/images/';
                $uploadPath = public_path($urlfile);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['url_fichier'] = $urlfile.$fileName;
            }
        }


        if ($request->hasFile('image_pub')) {
            if($file=$request['image_pub']){
                $urlfile='/uploads/publications/images/';
                $uploadPath = public_path($urlfile);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image_pub'] = $urlfile.$fileName;
            }
        }
            //dd($requestData);
       
        
       $user = User::WhereIn('id',$auteurs_array)->firstOrFail();
       $publication->auteurs()->sync($auteurs_array);
       $publication->labos()->sync($tab_centre);
       $publication->update($requestData);

        Session::flash('success', 'La Mise à jour de  <b>la Publication a été effectuée  !</b>');

        return redirect('administration/publication');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Publication::destroy($id);

        Session::flash('danger', 'La suppression de la <b>"Publication" a été effectuée !</b>');

        return redirect('administration/publication');
    }
}
