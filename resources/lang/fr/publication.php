<?php

return [
    'url_fichier' => 'Sélectionnez le fichier (format PDF)',
'image_pub' => "Image de couverture",
'titre' => 'Titre de la publication',
'coauteur' => 'Autres auteurs',
'auteurs' => 'Auteurs',
'description' => 'Description',
'annee_pub' => 'Année de publication',
'discipline_id' => 'Discipline',
'journal' => 'Revue/Journal',
'categorie_journal' => 'Discipline',
'impactfactor' => "Statut de la revue",
'volume' => 'Volume',
'numero' => 'Numéro (ISBN/ISSN)',
'resume' => 'Résumé de la publication',
'mots_cle' => 'Mots clés ',
'pages' => 'Références (pages, volume,...)',
'centre_id' => 'Centre ',
'type_document_id' => 'Type de Document',
'etat' => 'Etat',
'visibilite_id' => 'Visibilité',
];
