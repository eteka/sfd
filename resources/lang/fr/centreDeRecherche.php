<?php

return [
    'logo' => 'Logo',
'image_cover' => 'Image Cover',
'nom' => 'Nom',
'sigle' => 'Sigle',
'universite_id' => 'Université',
'description' => 'Description',
'contenu' => "Détails sur l'Université",
'etat' => 'Etat de validation du centre',
];
