@extends('layouts.web')
@section('content')
<div class="container bg-white margin-top-50 mt">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="">
                <div class="panel shadow1 panel-default ">
                    <div class="text-center "><div class="pad-all"></div>
                        <div class="center"> <a href="{{url('/')}}"><img alt="Université de Parakou" src="{{asset('assets/images/univ-parakou.png')}}" class="img-circle"> </a></div>
                        <div class="lock-wrapper">
                            @if(Auth::check())
                            <h4> Bienvenue {{Auth::user()->prenom}}  !</h4>
                            @endif
                        </div>
                        <p class="text-center text-muted">Que voulez vous faire ?</p>
                    </div>

                    <div class="panel-body ">
                        <a class="btn btn-default btn-block " href="{{url("admin")}}"> <i class="glyphicon glyphicon-cog text-primary"></i> <span class="text-primary"> Administration</span></a>
                        <a class="btn btn-default btn-block " href="{{url("/")}}"> <i class="glyphicon glyphicon-chevron-left text-primary"></i> <span class="text-primary"> Retour au site</span></a>
                    </div>
                    <div class="pad-all"></div>
                    <div class="pad-all"></div>
                </div>
                <div>
                    <br>
                    <div class="f11 text-xs text-center">
                        &copy;Université de Parakou {{date('Y')}}
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
