@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <ol class="breadcrumb mb30">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">Accueil </a></li>
                    <li class="breadcrumb-item"><a href="{{url('/administration')}}">Administration </a></li>
                    <li class="breadcrumb-item"> <a href="{{ url('/admin/promoteur/') }}">Promoteur</a></li>
                </ol>
            </div>
        </div>
        <div class="row">

            @include('admin.sidebar')

            <div class="col-md-10">
                <div class="content_main rond3">
                    <div class="panel panel-default ">
                        <div class="panel-heading">Promoteur</div>
                        <div class="panel-body">
                            <a href="{{ url('/admin/promoteur/create') }}" class="btn btn-success btn-sm mt-60 pull-right" title="Add New Promoteur">
                                <i class="fa fa-plus" aria-hidden="true"></i> Nouveau
                            </a>

                            {!! Form::open(['method' => 'GET', 'url' => '/admin/promoteur', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="form-control" value='{{ isset($_GET['search'])?$_GET['search']:NULL }}' name="search" placeholder="Rechercher...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            {!! Form::close() !!}

                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th>ID</th><th>{{ trans('promoteur.denomination') }}</th><th>{{ trans('promoteur.adresse') }}</th><th>{{ trans('promoteur.telephone') }}</th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=0;
                                    ?>
                                    @foreach($promoteur as $item)
                                    <?php
                                        $i=0;
                                    ?>
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->denomination }}</td><td>{{ $item->adresse }}</td><td>{{ $item->telephone }}</td>
                                            <td width='10%' nowrap="nowrap">
                                                <a href="{{ url('/admin/promoteur/' . $item->id) }}" title="View Promoteur"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Voir</button></a>
                                                <a href="{{ url('/admin/promoteur/' . $item->id . '/edit') }}" title="Modifier Promoteur"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/promoteur', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Suppression Promoteur',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if($i==0)
                                    <div class="pad20 well">
                                        Aucun donnée à afficher
                                    </div>
                                @endif
                                <div class="pagination-wrapper"> {!! $promoteur->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
