@extends('layouts.web')

@section('content')
    <div class="container">
            
            <div class="row">
                    <div class="col-md-3">   
                    @include('admin.menu')
                    </div>
                    <div class="col-md-9">
                    <div class="content_main rond3">
                    <div class="panel panel-default">
                        <div class="panel-heading">Ajout d'un nouveau Type</div>
                        <div class="panel-body">
                            <a href="{{ url('/admin/type') }}" title="Back"><button class="btn btn-warning btn-xs mt20"><i class="glyphicon glyphicon-arrow-left" aria-hidden="true"></i> Retour</button></a>
                            <br />
                            <br />

                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::open(['url' => '/admin/type', 'class' => 'form-horizontal', 'files' => true]) !!}

                            @include ('admin.type.form')

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
