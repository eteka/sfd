@extends('layouts.admin')

@section('content')
    <div class="container">
       
        <div class="row">
            <div class="col-md-3">   
            @include('admin.menu')
            </div>
            <div class="col-md-9">
                <div class="content_main rond3">
                    <div class="panel panel-default ">
                        <div class="panel-heading">Type</div>
                        <div class="panel-body">
                            <div class="row">   
                                <div class="col-sm-8">   
                                        {!! Form::open(['method' => 'GET', 'url' => '/admin/type', 'class' => 'navbar-form navbar- mt30', 'role' => 'search'])  !!}
                                            <!--div class="input-group ">
                                                <input type="text" class="form-control" value='{{ isset($_GET['search'])?$_GET['search']:NULL }}' name="search" placeholder="Rechercher...">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="glyphicon glyphicon-search"></i>
                                                    </button>
                                                </span>
                                            </div-->
                                            <div class="dropdown show">
                                              <!-- Actions Invoker -->
                                              <a id="basicTable1MenuInvoker" class="u-icon-sm link-muted" href="#" role="button" aria-haspopup="true" aria-expanded="true" data-toggle="dropdown" data-offset="8">
                                                  <span class="ti-more"></span>
                                              </a>
                                              <!-- End Actions Invoker -->

                                              
                                          </div>
                                            <div class="input-group h-100">
                                                <input class="form-control u-header-search__field" type="search" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                                                <button class="btn-link input-group-prepend u-header-search__btn" type="submit">
                                                <span class="ti-search"></span>
                                                 </button>
                                            </div>
                                            {!! Form::close() !!}
                                </div>
                                <div class="col-sm-4">
                                     <a href="{{ url('/admin/type/create') }}" class="btn btn-primary btn-sm text-uppercase mb-2 mr-2 mr-md-4 pull-right" title="Nouveau ">
                                <i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Ajouter
                            </a>
                                </div>
                            </div>
                           

                            
                            

                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Nom</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=1;
                                    ?>
                                    @foreach($type as $item)
                                   
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $item->nom }}</td>
                                            <td width='10%' nowrap="nowrap">
                                                <a href="{{ url('/admin/type/' . $item->id) }}" title="View Type"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Voir</button></a>
                                                <a href="{{ url('/admin/type/' . $item->id . '/edit') }}" title="Modifier Type"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/type', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Suppression Type',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if($i==0)
                                    <div class="pad20 well">
                                        Aucun donnée à afficher
                                    </div>
                                @endif
                                <div class="pagination-wrapper"> {!! $type->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
