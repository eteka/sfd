@extends('layouts.admin')

@section('content')
    <div >
        <h1 class="h2 mb-2">Promoteur</h1>

        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('/admin/promoteur/') }}">Promoteur</a></li>
            </ol>
        </nav>
        <div class="row">

            <div class="col-md-10 mb-5">
                <div class="h-100">
                    <div class="card h-100">
                        <header class="card-header">
                            <h2 class="h4 card-header-title"> Liste des Promoteur</h2>
                            
                        </header>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-md-8">
                                
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/promoteur', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
                            <div class="input-group h-100">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="ti-search"></span>
                                    </span>
                                </div>
                                <input class="form-control" type="search" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                                
                            </div>
                            {!! Form::close() !!}
                                </div>
                                <div class="col-md-3 text-center">
                                <a href="{{ url('/admin/promoteur/create') }}" class="btn btn-primary btn-sm mt-1 pull-rights " id="addbtns" title="Ajouter un Promoteur">
                                    <span class="ti-plus"></span> Créer nouveau
                                </a>
                                </div>
                            </div>

                            

                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-hover ">
                                    <thead>
                                        <tr>
                                            <th>#</th><th>{{ trans('promoteur.denomination') }}</th><th>{{ trans('promoteur.adresse') }}</th><th>{{ trans('promoteur.telephone') }}</th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=0;
                                    ?>
                                    @foreach($promoteur as $item)
                                    <?php
                                        $i=0;
                                    ?>
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->denomination }}</td><td>{{ $item->adresse }}</td><td>{{ $item->telephone }}</td>
                                            <!--td width='10%' nowrap="nowrap">
                                            <!-- Actions Menu >
                                              <div class="dropdown-menu dropdown-menu-right show" style="width: 150px; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-88px, -73px, 0px);" x-placement="top-end">
                                                  <div class="card border-0 p-3">
                                                      <ul class="list-unstyled mb-0">
                                                          <li class="mb-3">
                                                              <a href="{{ url('/admin/promoteur/' . $item->id) }}" title="View Promoteur"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Voir</a>
                                                          </li>
                                                          <li class="mb-3">
                                                              a href="{{ url('/admin/promoteur/' . $item->id . '/edit') }}" title="Modifier Promoteur"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                                          </li>
                                                          <li>
                                                              {!! Form::open([
                                                                'method'=>'DELETE',
                                                                'url' => ['/admin/promoteur', $item->id],
                                                                'style' => 'display:inline'
                                                            ]) !!}
                                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                                        'type' => 'submit',
                                                                        'class' => 'btn btn-danger btn-xs',
                                                                        'title' => 'Suppression Promoteur',
                                                                        'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                                )) !!}
                                                            {!! Form::close() !!}
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                              <!-- End Actions Menu >

                                            </td-->
                                            <td>
                                                <a href="{{ url('/admin/promoteur/' . $item->id) }}" title="Voir ce Promoteur"><i class="ti-eye" ></i> </a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ url('/admin/promoteur/' . $item->id . '/edit') }}" title="Modifier ce Promoteur"><i class="ti-pencil-alt"></i></a> 
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/promoteur', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="ti-trash" aria-hidden="true"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-sm text-danger btn-circle  btn-xs',
                                                            'title' => 'Suppression de ce Promoteur',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}

                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if($i==0)
                                    <div class="card well">
                                        Aucune donnée à afficher
                                    </div>
                                @endif
                                <div class="pagination-wrapper"> {!! $promoteur->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
