
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>@yield('title',"Répertoire des publications Scientiqiques ") | RPS</title>
        <meta charset="utf-8">
        <link rel="SHORTCUT ICON" href="{{asset('favicon.ico') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="ETEKA S. Wilfried" />
        <meta name="revisit-after" content="15" />
        <meta name="language" content="fr" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="identifier-url" content="http://www.univ-parakou.bj/" />
        <meta name="title" content="@yield('title',"Université de Parakou (Bénin)") " />
        <?php $desc = "Le répertoire des publications scientifiques est une Plateforme qui permet aux enseignants de l'Université de parakou de mettre en ligne les différents types publications  (articles, brevets, licences, communications, fiches techniques, lives, ...). Ces resources peuvent servir à d'autres travaux de recherche pour les étudiants et pour d'autres enseignants."; ?>
        <meta name="Description" content="@yield('description',$desc)"/> 
        

    
        <!-- Plugins CSS -->
        <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">     
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('css')
       
    </head>
    <body class="bge">

        @section('nav')
        
        @show
        @yield('content')
    </body>
</html>
